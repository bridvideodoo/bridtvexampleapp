//
//  ImageRendition.swift
//  BridTVExample
//
//  Created by Predrag Jevtic on 30.3.22..
//

import Foundation
import UIKit

class ImageRendition {
    
    func getBestImage(video: Video) -> String {
        
        if let image = video.snapshots {
            switch image {
            case .value(let snapshots):
                if let fhd = snapshots.fhd {
                    return fhd
                } else  if let hd = snapshots.hd {
                    return hd
                } else if let hsd = snapshots.hsd {
                    return hsd
                } else if let sd = snapshots.sd {
                    return sd
                } else if let ld = snapshots.ld {
                    return ld
                } else if let th = snapshots.th {
                    return th
                }
            case .error:
                if video.image != nil {
                    return video.image!
                }
            }
        }
        
        return ""
    }
}

extension UIImageView{
    
    func setImageFromURl(ImageUrl: String){
        if let url = NSURL(string: ImageUrl) {
            if let imagedata = NSData(contentsOf: url as URL) {
                self.image = UIImage(data: imagedata as Data)
            }
        }
    }
    
    func loadImageAsync(from urlString: String, completion: @escaping (UIImage?) -> Void) {
        guard let url = URL(string: urlString) else {
            completion(nil)
            return
        }

        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil, let image = UIImage(data: data) else {
                DispatchQueue.main.async {
                    completion(nil)
                }
                return
            }

            DispatchQueue.main.async {
                completion(image)
            }
        }.resume()
    }

}

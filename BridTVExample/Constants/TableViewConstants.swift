//
//  TableViewConstants.swift
//  BridTVExample
//
//  Created by Predrag Jevtic on 10.11.23..
//

import Foundation

struct TableViewConstants {
    struct HomeTableView {
        static let heightForRowAt: CGFloat = 275
        static let heightForHeaderInSection: CGFloat = 19
        static let numberOfRowsInSection = 1
    }
    struct SearchAndWatchlist {
        static let heightForRowAt: CGFloat = 172
    }
}

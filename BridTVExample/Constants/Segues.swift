//
//  Segues.swift
//  BridTVExample
//
//  Created by Predrag Jevtic on 10.11.23..
//

//MARK: - Frameworks
import Foundation

//MARK: - Segue Identifiers
struct Segues{
    static let toSearchVC = "toSearchVC"
    static let toDetailVC = "toDetailVC"
    static let searchToDetail = "searchToDetail"
    static let toRegisterVC = "toRegisterVC"
    static let loginToTabBarVC = "loginToTabBarVC"
    static let toResetPasswordVC = "toResetPasswordVC"
    static let logOutToLoginVC = "logOutToLoginVC"
    static let watchlistToDetail = "watchlistToDetail"
    static let playVideo = "toPlayerViewController"
}

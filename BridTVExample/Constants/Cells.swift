//
//  Cells.swift
//  BridTVExample
//
//  Created by Predrag Jevtic on 10.11.23..
//

//MARK: - Frameworks
import Foundation

//MARK: - Collection View Cell Identifiers
struct CollectionViewCells {
    static let nowPlayingCell = "nowPlayingCell"
    static let castCell = "castCell"
    static let trailerCell = "trailerCell"
}

//MARK: - Table View Cell Identifiers
struct TableViewCells {
    static let movieTableViewCell = "movieTableViewCell"
    static let searchTableViewCell = "searchTableViewCell"
    static let settingsCell = "settingsCell"
    static let watchlistTableViewCell = "watchlistTableViewCell"
}

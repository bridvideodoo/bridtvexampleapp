//
//  WatchlistGlobal.swift
//  BridTVExample
//
//  Created by Predrag Jevtic on 24.11.23..
//

import Foundation
import Firebase
import FirebaseAuth


class WatchlistGlobal {
    static let shared = WatchlistGlobal()
    let db = Firestore.firestore()
    var watchlistArray: [Watchlist] = []

    private init() {
        fetchData()
    }

    func fetchData() {
        if FirebaseAuth.Auth.auth().currentUser != nil {
            db.collection("users")
                .document(Auth.auth()
                    .currentUser!.uid)
                .collection("watch_list")
                .order(by: "date_added", descending: true)
                .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error: \(err)")
                } else {
                    self.watchlistArray = []
                    for document in querySnapshot!.documents {
                        do {
                            let watchlistItem = try document.data(as: Watchlist.self)
                            self.watchlistArray.append(watchlistItem)
                        } catch let error {
                            print("Error: \(error)")
                        }
                    }
                }
            }
        }
    }
    
    func isInWatchList(videoId: String) ->Bool {
        for video in watchlistArray {
            if video.videoId == videoId {
                return true
            }
        }
        return false
    }
    
}

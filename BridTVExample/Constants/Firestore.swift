//
//  Firestore.swift
//  BridTVExample
//
//  Created by Predrag Jevtic on 10.11.23..
//

//MARK: - Frameworks
import Foundation

//MARK: - FirestoreConstants
struct FirestoreConstants{
    static let collectionName = "Watchlist"
    static let movieId = "movieId"
    static let id = "id"
    static let title = "title"
    static let date = "date"
    static let score = "score"
    static let overview = "overview"
    static let posterPath = "poster_path"
    static let uploadDate = "upload_date"
    static let email = "email"
    static let uuid = "UUID"
}

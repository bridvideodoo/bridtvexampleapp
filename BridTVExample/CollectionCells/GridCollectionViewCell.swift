//
//  GridCollectionViewCell.swift
//  BridTVExample
//
//  Created by Predrag Jevtic on 25.9.23..
//

import UIKit
import SDWebImage

class GridCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cellBtn: UIButton!
    @IBOutlet weak var gradientView: UIView!
    var imageRendition = ImageRendition()
    var currentVideo: Video?
    var currentPlayer: String?
    @IBOutlet weak var headerTitleLabel: UILabel!
    
    static let identifier = "GridCollectionViewCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "GridCollectionViewCell", bundle: nil)
    }
    
    var didSelectItemAction: ((Video, String) -> Void)?
    
    @IBAction func selectedCellPressed(_ sender: Any) {
        if let playerId = currentPlayer, let video = currentVideo {
            didSelectItemAction?(video, playerId)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cellBtn.setTitle("", for: .normal)
    }
    
    public func configure(with model:Video, playerID:String) {
        currentVideo = model
        currentPlayer = playerID
        if let name = model.name {
            self.titleLabel.text = name
            if let url = URL(string: imageRendition.getBestImage(video: model)) {
                self.imageView.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
            } else {
                self.imageView.image = UIImage(named: "placeholder")
            }
        }
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView?.image = nil
    }

}

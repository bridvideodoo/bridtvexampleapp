//
//  SliderCollectionCell.swift
//  BridTVExample
//
//  Created by Predrag Jevtic on 9.11.23..
//

import UIKit

class SliderCollectionCell: UICollectionViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var headerTitleLabel: UILabel!
    
    var models = [Video]()
    var playerID: String?
    var cellheight: CGFloat?
    static let identifier = "SliderCollectionCell"
    var lastSelectedIndexPath: IndexPath?
    static func nib() -> UINib {
        return UINib(nibName: "SliderCollectionCell", bundle: nil)
    }
    
    var didSelectItemAction: ((Video, String) -> Void)?
    
    func configure(with models:[Video], playerID:String, channel: String)
    {
        self.playerID = playerID
        if models.count != 0 {
            self.models = models
            if !(models.count < 1) {
                collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .left, animated: true)
                collectionView.translatesAutoresizingMaskIntoConstraints = false
            }
            collectionView.reloadData()
        }
       
    }
    
    //MARK: - Life Cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.register(SliderCollectionViewCell.nib(), forCellWithReuseIdentifier: SliderCollectionViewCell.identifier)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    //MARK: - CollectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return models.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SliderCollectionViewCell.identifier, for: indexPath) as! SliderCollectionViewCell
        cell.configure(with: models[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if indexPath != lastSelectedIndexPath {
            if let playerID = playerID {
                didSelectItemAction?(models[indexPath.row], playerID)
            }
//            lastSelectedIndexPath = indexPath
//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let heightCollection = self.collectionView.frame.size.height
        let size = CGSize(width: (heightCollection * 0.85) * 16/9, height: heightCollection)
        return size
    }

}

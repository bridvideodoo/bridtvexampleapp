//
//  TopCollectionViewCell.swift
//  BridTVExample
//
//  Created by Predrag Jevtic on 9.11.23..
//

import UIKit

class TopCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cellBtn: UIButton!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var nuberLabel: UILabel!
    @IBOutlet weak var headerTitleLabel: UILabel!
    var imageRendition = ImageRendition()
    var currentVideo: Video?
    var currentPlayer: String?
    
    static let identifier = "TopCollectionViewCell"
    
    var didSelectItemAction: ((Video, String) -> Void)?
    
    static func nib() -> UINib {
        return UINib(nibName: "TopCollectionViewCell", bundle: nil)
    }
    
    @IBAction func selectedCellPressed(_ sender: Any) {
        if let playerId = currentPlayer, let video = currentVideo {
            didSelectItemAction?(video, playerId)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cellBtn.setTitle("", for: .normal)
    }
    
    public func configure(with model:Video, number: Int?, playerID:String) {
        currentVideo = model
        currentPlayer = playerID
        if let name = model.name {
            self.titleLabel.text = name
            self.nuberLabel.text = String(number ?? 0)
            if let url = URL(string: imageRendition.getBestImage(video: model)) {
                self.imageView.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
            } else {
                self.imageView.image = UIImage(named: "placeholder")
            }
        }
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView?.image = nil
    }

}

//
//  CaruselCollectionCell.swift
//  BridTVExample
//
//  Created by Predrag Jevtic on 9.11.23..
//

import UIKit

class CaruselCollectionCell: UICollectionViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var models = [Video]()
    var playerID: String?
    var cellheight: CGFloat?
    var timer = Timer()
    var counter = 0
    
    static let identifier = "CaruselCollectionCell"
    var lastSelectedIndexPath: IndexPath?
    
    static func nib() -> UINib {
        return UINib(nibName: "CaruselCollectionCell", bundle: nil)
    }
    
    var didSelectItemAction: ((Video, String) -> Void)?
    
    func configure(with models:[Video], playerID:String, channel: String)
    {
        if !areModelsEqual(models, self.models) {
            self.playerID = playerID
            if models.count != 0 {
                self.models = models
                if !(models.count < 1) {
                    collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .left, animated: true)
                    collectionView.translatesAutoresizingMaskIntoConstraints = false
                    pageControl.numberOfPages = models.count
                    pageControl.currentPage = 0
                    collectionView.reloadData()
                }
                
                
                DispatchQueue.main.async {
                    self.timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.scrollModel), userInfo: nil, repeats: true)
                }
            }
        }
        
    }
    
    @objc func scrollModel() {
        
        if counter < models.count {
            let index = IndexPath.init(item: counter, section: 0)
            self.collectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
            pageControl.currentPage = counter
            counter += 1
        } else {
            counter = 0
            let index = IndexPath.init(item: counter, section: 0)
            self.collectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: false)
            pageControl.currentPage = counter
            counter = 1
        }
        
    }
    
    func areModelsEqual(_ models1: [Video], _ models2: [Video]) -> Bool {
        if models1.count != models2.count {
            return false
        }
        
        for (index, model1) in models1.enumerated() {
            let model2 = models2[index]
            
            if model1.id != model2.id || model1.name != model2.name {
                return false
            }
        }
        
        return true
    }

    //MARK: - Life Cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.register(CarusellCollectionViewCell.nib(), forCellWithReuseIdentifier: CarusellCollectionViewCell.identifier)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    //MARK: - CollectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return models.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CarusellCollectionViewCell.identifier, for: indexPath) as! CarusellCollectionViewCell
        cell.configure(with: models[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if indexPath != lastSelectedIndexPath {
            if let playerID = playerID {
                didSelectItemAction?(models[indexPath.row], playerID)
            }
//            lastSelectedIndexPath = indexPath
//        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize(width: self.collectionView.frame.size.width, height: self.collectionView.frame.size.height)
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let visibleRect = CGRect(origin: self.collectionView.contentOffset, size: self.collectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        if let visibleIndexPath = self.collectionView.indexPathForItem(at: visiblePoint) {
            self.pageControl.currentPage = visibleIndexPath.row
            counter = visibleIndexPath.row
        }
    }

}

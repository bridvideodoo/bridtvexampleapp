//
//  ChannelCollectionViewCell.swift
//  BridTVExample
//
//  Created by Predrag Jevtic on 25.1.22..
//

import UIKit
import SDWebImage

class SliderCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var duration: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    var imageRendition = ImageRendition()
    
    static let identifier = "SliderCollectionViewCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "SliderCollectionViewCell", bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func configure(with model:Video) {
        if let name = model.name {
            self.titleText.text = name
            if let url = URL(string: imageRendition.getBestImage(video: model)) {
                self.imageView.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
            } else {
                self.imageView.image = UIImage(named: "placeholder")
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView?.image = nil
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        let targetSize = CGSize(width: layoutAttributes.frame.width, height: 0)
        layoutAttributes.frame.size = contentView.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: .required, verticalFittingPriority: .fittingSizeLevel)
        return layoutAttributes
    }
    
}

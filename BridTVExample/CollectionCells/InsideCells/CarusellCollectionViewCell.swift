//
//  CarusellCollectionViewCell.swift
//  BridTVExample
//
//  Created by Predrag Jevtic on 4.8.23..
//

import UIKit


class CarusellCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var cntImage: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var gradientView: UIView!
    var imageRendition = ImageRendition()
    
    static let identifier = "CarusellCollectionViewCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "CarusellCollectionViewCell", bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    public func configure(with model:Video) {
        if let name = model.name {
            self.titleLbl.text = name
            self.infoLbl.text = model.description
            if model.image != nil {
                self.cntImage.downloaded(from: imageRendition.getBestImage(video: model))
            } else {
                self.cntImage.image = UIImage(named: "placeholder")
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        cntImage?.image = nil
    }

}

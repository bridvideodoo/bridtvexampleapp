//
//  ChannelData.swift
//  BridTVWhiteLabel
//
//  Created by Xcode Peca on 26/11/2020.
//

import Foundation

struct PlaylistData: Codable {
    
    var Playlist: Playlist
    var Player: Player?
    var Video: [Video]
    
    init(Playlist: Playlist, Player: Player, Video: [Video]) {
            self.Playlist = Playlist
            self.Player = Player
            self.Video = Video
        }
}


//
//  Watchlist.swift
//  BridTVExample
//
//  Created by Predrag Jevtic on 23.11.23..
//

import Foundation

struct Watchlist: Codable {

    var date_added: String?
    var description: String?
    var duration: String?
    var playerId: String?
    var title: String?
    var tubmnailUrl: String?
    var videoId: String?
   
}

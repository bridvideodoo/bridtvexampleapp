//
//  Layout.swift
//  BridTVExample
//
//  Created by Predrag Jevtic on 13.10.23..
//

import Foundation

enum Layout: String, Codable {
    case slider = "slider"
    case carusel = "carusel"
    case grid = "grid"
    case top = "top"
    
    func cellSize(for width: CGFloat) -> CGSize {
            switch self {
            case .slider:
                return CGSize(width: width, height: 200)
            case .carusel:
                return CGSize(width: width, height: 400)
            case .grid:
                let cellWidth = (width - 15) / 2
                return CGSize(width: cellWidth, height: 200)
            case .top:
                return CGSize(width: width, height: 200)
            }
        }
    
    init(from decoder: Decoder) throws {
           let container = try decoder.singleValueContainer()
           let rawValue = try container.decode(String.self)

           if let layout = Layout(rawValue: rawValue) {
               self = layout
           } else {
               self = .slider
           }
       }
}

//
//  Featured.swift
//  BridTVExample
//
//  Created by Predrag Jevtic on 21.9.23..
//

import Foundation

struct Featured:Codable {
    
    var layout: String?
    var playerId: String?
    var type: String?
    var items: Int?
    var page: Int?
    var id: String?
    var name: String?
    
    init(layout: String?, playerId: String?, type: String?, items: Int?, page: Int?, id: String?, name: String?) {
        self.layout = layout
        self.playerId = playerId
        self.type = type
        self.items = items
        self.page = page
        self.id = id
        self.name = name
    }
    
    
}

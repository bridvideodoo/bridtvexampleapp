//
//  Page.swift
//  BridTVExample
//
//  Created by Predrag Jevtic on 21.9.23..
//

import Foundation

struct Page:Codable {
    
    var label: String?
    var description: String?
    var type: String?
    var creatorName: String?
    var path: String?
    var playlists: [Playlist]
    var playlistsData: [PlaylistData]?
    var playlistsUrls: [String]?
    
    init(label: String?, description: String?, type: String?, creatorName: String?, path: String?, playlists: [Playlist], playlistsData: [PlaylistData]?, playlistsURLs: [String]) {
            self.label = label
            self.description = description
            self.type = type
            self.creatorName = creatorName
            self.path = path
            self.playlists = playlists
            self.playlistsData = playlistsData
            self.playlistsUrls = playlistsURLs
        }
}

//
//  Playlist.swift
//  BridTVExample
//
//  Created by Predrag Jevtic on 20.7.22..
//

import Foundation

struct Playlist: Codable {
    
    var layout: Layout?
    var playerId: String?
    var type: String?
    var items: Int?
    var page: Int?
    var id: String?
    var path: String?
    var name: String?
    var limit: Int?
    
    init(layout: Layout?, playerId: String?, type: String?, items: Int?, page: Int?, id: String?, path: String?, name: String?, limit: Int? ) {
        self.layout = layout
        self.playerId = playerId
        self.type = type
        self.items = items
        self.page = page
        self.id = id
        self.path = path
        self.name = name
        self.limit = limit
    }
    
}

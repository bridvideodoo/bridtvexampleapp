//
//  PagesViewModel.swift
//  BridTVExample
//
//  Created by Predrag Jevtic on 10.11.23..
//

import Foundation

struct PagesViewModel: Codable {

    var label: String?
    var description: String?
    var pageViewModel: [PageViewModel]?
   
}

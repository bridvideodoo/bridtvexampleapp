//
//  User.swift
//  BridTVExample
//
//  Created by Predrag Jevtic on 29.11.23..
//

import Foundation

struct User: Codable {
    var name: String
    var image: String
    var email: String
    var active: Bool
    var dateOfBirth: String
}

//
//  ViewModelPage.swift
//  BridTVExample
//
//  Created by Predrag Jevtic on 10.11.23..
//

import Foundation

struct PageViewModel: Codable {
    
    static var currentTopNumber = 0

    var layout: Layout
    var name: String?
    var player: String?
    var videos: [Video]
    var topNumber: Int?
    var video: Video?
    var isFirstOfItsType: Bool = false
    
    static func resetTopNumber() {
        currentTopNumber = 0
    }

    init(layout: Layout, name: String?, player: String?, videos: [Video], video: Video?) {
        self.layout = layout
        self.name = name
        self.player = player
        self.videos = videos
        self.video = video

        if layout == .top {
            PageViewModel.currentTopNumber += 1
            self.topNumber = PageViewModel.currentTopNumber
        }
    }
}

//
//  RegisterViewController.swift
//  BridTVExample
//
//  Created by Predrag Jevtic on 10.11.23..
//

//MARK: - Frameworks
import UIKit
import FirebaseAuth
import GoogleSignIn
import Firebase

//MARK: - RegisterViewController
class RegisterViewController: UIViewController {
    
    //MARK: - Outlets
    //Properties
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var passwordHideAndShowButton: UIButton!
    @IBOutlet weak var confirmPassHideAndShowButton: UIButton!
    
    let db = Firestore.firestore()
    
    //MARK: - Lifecycle
    //viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        closeKeyboard()
    }
    
    //MARK: - Actions
    //Password Hide and Show Button
    @IBAction func passwordHideAndShowPressed(_ sender: UIButton) {
        hideAndShowButtonConfiguration(textField: passwordTextField, button: passwordHideAndShowButton)
    }
    //Confirm Password Hide and Show Button
    @IBAction func confirmPasswordHideAndShowPressed(_ sender: UIButton) {
        hideAndShowButtonConfiguration(textField: confirmPasswordTextField, button: confirmPassHideAndShowButton)
    }
    //Register Button
    @IBAction func registerButtonPressed(_ sender: UIButton) {
        if let email = emailTextField.text, let password = passwordTextField.text, let confirmPass = confirmPasswordTextField.text {
            if password == confirmPass {
                Auth.auth().createUser(withEmail: email, password: password){_, error in
                    if let error {
                        self.alertMessage(alertTitle: "Error", alertMesssage: error.localizedDescription, completionHandler: nil)
                    }else{
                        self.navigationController?.popViewController(animated: true)
                        self.alertMessage(alertTitle: "Success", alertMesssage: "You have successfully registered.") {
                            guard let currentUser = Auth.auth().currentUser else { return }
                            let user = User(name: "", image: "", email: currentUser.email ?? "", active: true, dateOfBirth: "")
                            do {
                                try self.db.collection("users").document(currentUser.uid).setData(from: user){ error in
                                    if let error = error {
                                        self.alertMessage(alertTitle: "Error", alertMesssage: "\(error)", completionHandler: nil)
                                    }
                                }
                            } catch let error {
                                self.alertMessage(alertTitle: "Error", alertMesssage: "\(error)", completionHandler: nil)
                            }
                            
                            DispatchQueue.main.async {
                                if let tabBarController = self.tabBarController {
                                    tabBarController.selectedIndex = 0
                                }
                            }
                        }
                    }
                }
               
            }else{
                alertMessage(alertTitle: "Error", alertMesssage: "Passwords are not match!", completionHandler: nil)
            }
        }else{
            alertMessage(alertTitle: "Error", alertMesssage: "Please fill all the necessary parts!", completionHandler: nil)
        }
    }
    
    //Sign In Button
    @IBAction func signInButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}


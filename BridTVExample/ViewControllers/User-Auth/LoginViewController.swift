//
//  LoginViewController.swift
//  BridTVExample
//
//  Created by Predrag Jevtic on 10.11.23..
//

//MARK: - Frameworks
import UIKit
import FirebaseAuth
import GoogleSignIn
import FirebaseCore
import Firebase

//MARK: - LoginViewController
class LoginViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var dateOfBirthTextField: UITextField!
    @IBOutlet weak var hideAndShowButton: UIButton!
    @IBOutlet weak var rememberMeButton: UIButton!
    @IBOutlet weak var logoutViewLogo: UIView!
    @IBOutlet weak var loginViewLogo: UIView!
    @IBOutlet weak var fieldsLogView: UIView!
    @IBOutlet weak var fieldsSettingView: UIView!
    @IBOutlet weak var logoutButton: UIView!
    @IBOutlet weak var loginButton: UIView!
    
    //MARK: - Objects
    var rememberMe = false
    let datePicker = UIDatePicker()
    let db = Firestore.firestore()
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setNavigationLogo()
        checkRememberMe() //Remember Me buton activated.
        closeKeyboard()
        createDatePicker()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setupView()
        loadSettings()
        
    }
    
    func loadSettings() {
        if FirebaseAuth.Auth.auth().currentUser != nil {
            db.collection("users").document(Auth.auth().currentUser!.uid).getDocument() { (querySnapshot, err) in
                if let err = err {
                    print("Error: \(err)")
                } else {
                    if let querySnapshot = querySnapshot, querySnapshot.exists,
                            let name = querySnapshot.data()?["name"] as? String {
                        self.nameTextField.text = name
                    }
                    if let querySnapshot = querySnapshot, querySnapshot.exists, let date = querySnapshot.data()?["dateOfBirth"] as? String {
                        self.dateOfBirthTextField.text = date
                    }
                }
            }
        }
    }
    
    func createDatePicker() {
            datePicker.datePickerMode = .date

            if #available(iOS 14, *) {
                datePicker.preferredDatePickerStyle = .wheels
            }

            dateOfBirthTextField.inputView = datePicker

            let toolbar = UIToolbar()
            toolbar.sizeToFit()

            let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
            toolbar.setItems([doneButton], animated: true)

            dateOfBirthTextField.inputAccessoryView = toolbar
        }

        @objc func donePressed() {
            // Formatiranje datuma za prikaz u textField-u
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            dateFormatter.timeStyle = .none

            dateOfBirthTextField.text = dateFormatter.string(from: datePicker.date)
            self.view.endEditing(true) // Sakrivanje datePicker-a i toolbar-a
        }

    
    //MARK: - Actions
    //Forgot password button
    @IBAction func forgotPasswordPressed(_ sender: UIButton) {
        performSegue(withIdentifier: Segues.toResetPasswordVC, sender: nil)
    }
    //Login buton
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: nil, message: "Please wait\n\n", preferredStyle: .alert)
        let spinnerIndicator = UIActivityIndicatorView(style: .large)
        spinnerIndicator.center = CGPoint(x: 135.0, y: 65.5)
        spinnerIndicator.color = UIColor.black
        spinnerIndicator.startAnimating()
        alertController.view.addSubview(spinnerIndicator)
        self.present(alertController, animated: false, completion: nil)
        
        if let email = emailTextField.text, let password = passwordTextField.text {
            Auth.auth().signIn(withEmail: email, password: password) { [weak self] authResult, error in
                guard let strongSelf = self else { return }
                if let error {
                    alertController.dismiss(animated: true, completion: nil);
                    strongSelf.alertMessage(alertTitle: "Error", alertMesssage: error.localizedDescription, completionHandler: nil)
                }else{
                    if strongSelf.rememberMe == true {
                        UserDefaults.standard.set("1", forKey: "RememberMe")
                        UserDefaults.standard.set(email, forKey: "Email")
                        UserDefaults.standard.set(password, forKey: "Password")
                    }else if strongSelf.rememberMe == false {
                        UserDefaults.standard.set("2", forKey: "RememberMe")
                    }
                    alertController.dismiss(animated: true, completion: nil);
                    _ = WatchlistGlobal.shared
                    WatchlistGlobal.shared.fetchData()
                   
                    self!.alertMessage(alertTitle: "Success", alertMesssage: "You have successfully logged in.") {
                        if let tabBarController = self?.tabBarController {
                            tabBarController.selectedIndex = 0
                        }
                    }
                    self!.setupView()
                }
            }
        }
    }
    
    @IBAction func googleSingInTapped(_ sender: Any) {
        signInWithGoogle()
    }
    
    @objc func signInWithGoogle() {
        guard let clientID = FirebaseApp.app()?.options.clientID else { return }
        
        let alertController = UIAlertController(title: nil, message: "Please wait\n\n", preferredStyle: .alert)
        
        let config = GIDConfiguration(clientID: clientID)
        GIDSignIn.sharedInstance.signIn(with: config, presenting: self) { [unowned self] user, error in
            
            if let error = error {
                self.alertMessage(alertTitle: "Error", alertMesssage: error.localizedDescription, completionHandler: nil)
                return
            }
            
            guard let authentication = user?.authentication, let idToken = authentication.idToken else {
                return
            }
            
            let credential = GoogleAuthProvider.credential(withIDToken: idToken, accessToken: authentication.accessToken)
            
            Auth.auth().signIn(with: credential) { [weak self] authResult, error in
                if let error = error {
                    self!.alertMessage(alertTitle: "Error", alertMesssage: error.localizedDescription, completionHandler: nil)
                    return
                }
                _ = WatchlistGlobal.shared
                WatchlistGlobal.shared.fetchData()
                self!.alertMessage(alertTitle: "Success", alertMesssage: "You have successfully logged in.") {
                    if let tabBarController = self?.tabBarController {
                        tabBarController.selectedIndex = 0
                    }
                }
                self!.setupView()
            }
        }
    }
    
    //Register button
    @IBAction func registerButtonPressed(_ sender: UIButton) {
        performSegue(withIdentifier: Segues.toRegisterVC, sender: nil)
    }
    //Password hide and show button
    @IBAction func hideAndShowPasswordButtonPressed(_ sender: UIButton) {
        hideAndShowButtonConfiguration(textField: passwordTextField, button: hideAndShowButton)
    }
    
    @IBAction func logoutBtnTapped(_ sender: Any) {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            DispatchQueue.main.async {
                self.setupView()
            }
        } catch let signOutError as NSError {
            print("Error signing out: %@", signOutError)
        }
        
        WatchlistGlobal.shared.watchlistArray.removeAll()
       
    }
    @IBAction func SaveChangesTapped(_ sender: Any) {
        guard let currentUser = Auth.auth().currentUser else { return }
        let user = User(name: nameTextField.text ?? "", image: "", email: currentUser.email ?? "", active: true, dateOfBirth: dateOfBirthTextField.text ?? "")
        do {
            try self.db.collection("users").document(currentUser.uid).setData(from: user){ error in
                if let error = error {
                    self.alertMessage(alertTitle: "Error", alertMesssage: "\(error)", completionHandler: nil)
                } else {
                    self.alertMessage(alertTitle: "Success", alertMesssage: "You saved data", completionHandler: nil)
                }
            }
        } catch let error {
            self.alertMessage(alertTitle: "Error", alertMesssage: "\(error)", completionHandler: nil)
        }
    }
    
    //Remember me button
    @IBAction func rememberMeButtonPressed(_ sender: UIButton) {
        if rememberMe == false {
            rememberMe = true
            rememberMeButton.setImage(UIImage(systemName: "checkmark"), for: .normal)
            rememberMeButton.alpha = 1
        }else if rememberMe == true {
            rememberMe = false
            rememberMeButton.setImage(UIImage(named: "tickbox"), for: .normal)
            rememberMeButton.alpha = 0.4
        }
    }
    
    //MARK: - Methods
    //Check remember me box
    func checkRememberMe(){
        let defaults = UserDefaults.standard
        if defaults.string(forKey: "RememberMe") == "1" {
            rememberMeButton.setImage(UIImage(systemName: "checkmark"), for: .normal)
            rememberMeButton.alpha = 1
            rememberMe = true
            self.emailTextField.text = defaults.string(forKey: "Email")
            self.passwordTextField.text = defaults.string(forKey: "Password")
        }else {
            rememberMeButton.setImage(UIImage(named: "tickbox"), for: .normal)
            rememberMeButton.alpha = 0.4
            rememberMe = false
        }
    }
    
    func setupView() {
        if FirebaseAuth.Auth.auth().currentUser != nil {
            loginButton.isHidden = true
            logoutButton.isHidden = false
            fieldsLogView.isHidden = true
            fieldsSettingView.isHidden = false
            loginViewLogo.isHidden = true
            logoutViewLogo.isHidden = false
            logoutButton.isHidden = false
        } else {
            loginButton.isHidden = false
            logoutButton.isHidden = true
            fieldsLogView.isHidden = false
            fieldsSettingView.isHidden = true
            loginViewLogo.isHidden = false
            logoutViewLogo.isHidden = true
            logoutButton.isHidden = true
        }
    }
}

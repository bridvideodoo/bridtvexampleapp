//
//  InfoViewController.swift
//  BridTVExample
//
//  Created by Predrag Jevtic on 26.1.22..
//

import UIKit
import BridSDK
import Firebase
import FirebaseAuth

class PlayerViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - Setting local varibale for dataSource
    
    @IBOutlet weak var viewForPlayer: UIView!
    @IBOutlet weak var duratinLabel: UILabel!
    @IBOutlet weak var descriptionText: UITextView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var reloadingCollectionView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var favoriteBtn: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentViewHeight: NSLayoutConstraint!
    
    var video: Video?
    var playerId: String?
    var apiRequest = ApiRequest()
    var sendData: Video?
    var pagePlayer: BVPlayer!
    let db = Firestore.firestore()
    
    var related: SingleVideo! {
        didSet {
            tableView.register(SliderTableViewCell.nib(), forCellReuseIdentifier: SliderTableViewCell.identifier)
            tableView.delegate = self
            tableView.dataSource = self
            tableView.reloadData()
            
            UIView.transition(with: reloadingCollectionView, duration: 0.5,
                              options: .transitionCrossDissolve,
                              animations: {
                self.reloadingCollectionView.isHidden = false
                
            })
        }
    }
    
    //MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pagePlayer = nil
//        setNavigationLogo()
        
        if let playerID = playerId, let videoId = video?.id {
            pagePlayer = BVPlayer(data: BVData(playerID: Int32(playerID)!, forVideoID: Int32(videoId)!), for: viewForPlayer)
            setupEventNetworking()
        }
        
        if let text = video?.description, let duration = video?.duration {
            self.descriptionText.text = text
            self.duratinLabel.text = "\(secondsToHoursMinutesSeconds(duration))"
            adjustUITextViewHeight(arg: descriptionText)
        }
        
        if WatchlistGlobal.shared.isInWatchList(videoId: (video?.id)!) {
            favoriteBtn.setImage(UIImage(systemName: "bookmark.fill"), for: .normal)
        } else {
            favoriteBtn.setImage(UIImage(systemName: "bookmark"), for: .normal)
        }
        
        overrideUserInterfaceStyle = .dark
        
        self.descriptionText.translatesAutoresizingMaskIntoConstraints = true
        
        //        switch traitCollection.userInterfaceStyle {
        //        case .light, .unspecified:
        self.navigationController?.navigationBar.tintColor = UIColor.white
        //        case .dark:
        //            self.navigationController?.navigationBar.tintColor = UIColor.white
        //        @unknown default:
        //            self.navigationController?.navigationBar.tintColor = UIColor.blue
        //        }
        
        getRelatedVideo()
        tableView.isUserInteractionEnabled = false
    }
    
    deinit {
        self.dismiss(animated: true)
        pagePlayer?.stop()
        pagePlayer.destroy()
        removeFromParent()
        viewForPlayer.removeFromSuperview()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        setScrollHeight()
    }
    
    func getRelatedVideo() {
        if let videoId = video?.id {
            apiRequest.getRelatedVideo(playerID: playerId!, videoID: videoId) { (result) in
                switch result {
                case .success(let model):
                    self.related = model
                case .failure(let error):
                    debugPrint(error.localizedDescription)
                }
            }
        }
    }
    
    
    //MARK - ScrollView size height
    func setScrollHeight() {
        
        let fixedWidth = view.frame.size.width - 50
        descriptionText.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        var newFrame = descriptionText.frame
        let newSize = descriptionText.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        
        descriptionText.frame = newFrame;
        let pageHeight = 280 + descriptionText.frame.height
        if pageHeight < 330 {
            scrollView.isScrollEnabled = false
        } else {
            scrollView.isScrollEnabled = true
        }
        
        self.contentViewHeight.constant = pageHeight
        scrollView.setContentOffset(CGPoint.zero, animated: true)
    }
    
    //MARK - Reload data
    func reloadViewController(video: Video) {
        tableView.isUserInteractionEnabled = false
        self.loadingView.isHidden = false
        reloadingCollectionView.isHidden = true
        
        getRelatedVideo()
        
        if let text = video.description, let duration = video.duration {
            self.descriptionText.text = text
            setScrollHeight()
            self.duratinLabel.text = "\(secondsToHoursMinutesSeconds(duration))"
        }
        
        if let playerID = playerId, let videoId = video.id {
            pagePlayer = BVPlayer(data: BVData(playerID: Int32(playerID)!, forVideoID: Int32(videoId)!), for: viewForPlayer)
            setupEventNetworking()
        }
        
    }
    
    func adjustUITextViewHeight(arg : UITextView) {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = false
    }
    
    //MARK: - BVPlayer events
    
    func setupEventNetworking() {
        NotificationCenter.default.addObserver(self, selector: #selector(eventWriter), name:NSNotification.Name(rawValue: "PlayerEvent"), object: nil)
    }
    
    @objc func eventWriter(_ notification: NSNotification) {
        
        if notification.name.rawValue == "PlayerEvent" {
            
            let event = notification.userInfo?["event"] as! String
            if (event) == "playerVideoLoad" {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.loadingView.isHidden = true
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    self.tableView.isUserInteractionEnabled = true
                }
            }
        }
    }
    
    @IBAction func favoriteBtnTapped(_ sender: Any) {
        if FirebaseAuth.Auth.auth().currentUser == nil {
            showAlert(title: "Not allowed", message: "Login to save on list", buttonTitle: "Go to Login") {
                if let tabBarController = self.tabBarController {
                    tabBarController.selectedIndex = 2
                }
            }
            pagePlayer?.stop()
        } else {
            if (WatchlistGlobal.shared.isInWatchList(videoId: (video?.id)!)) {
                favoriteBtn.setImage(UIImage(systemName: "bookmark"), for: .normal)
                let documentReference = db.collection("users").document(Auth.auth().currentUser!.uid).collection("watch_list").document((video?.id)!)
                documentReference.delete() { err in
                    if let err = err {
                        print("Error removing document: \(err)")
                    } else {
                        print("Document successfully removed!")
                    }
                    WatchlistGlobal.shared.fetchData()
                }
            } else {
                db.collection("users").document(Auth.auth().currentUser!.uid).collection("watch_list").document((video?.id)!).setData([
                    "date_added":  String(DateFormatter.localizedString(from: Date(), dateStyle: .medium, timeStyle: .short)),
                    "description": video?.description as Any,
                    "duration": video?.duration as Any,
                    "playerId":  self.playerId as Any,
                    "title": video?.name as Any,
                    "tubmnailUrl":video?.image as Any,
                    "videoId": video?.id as Any
                ]) { err in
                    if let err = err {
                        print("Error writing document: \(err)")
                    } else {
                        print("Document successfully written!")
                    }
                }
                favoriteBtn.setImage(UIImage(systemName: "bookmark.fill"), for: .normal)
                WatchlistGlobal.shared.fetchData()
            }
        }
    }
    
    //MARK: - TableView Delegates
    
    func tableView(_ tableView: UITableView, canFocusRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let relatedCell = tableView.dequeueReusableCell(withIdentifier: SliderTableViewCell.identifier, for: indexPath) as! SliderTableViewCell
        relatedCell.configure(with: related.Related, playerID: related.Player.id, channel: "Related")
        relatedCell.cellheight = relatedCell.frame.size.height
        relatedCell.didSelectItemAction = { [weak self] video, playerId in
            self?.pagePlayer = nil
            self?.video = video
            self?.playerId = playerId
            self!.reloadViewController(video: self!.video!)
        }
        relatedCell.layoutIfNeeded()
        
        return relatedCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableView.bounds.height
    }
    
    //MARK - Draw minutes from string
    func secondsToHoursMinutesSeconds(_ seconds: String) -> (String) {
        if let time = Int(seconds) {
            return time.convertSecondsToHrMinuteSec(seconds: time)
        }
        return ""
    }
    
}

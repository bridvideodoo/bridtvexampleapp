//
//  SearchViewController.swift
//  BridTVExample
//
//  Created by Predrag Jevtic on 10.11.23..
//

//MARK: - Frameworks
import UIKit

//MARK: - SearchViewController
class SearchViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchTableView: UITableView!
    
    //MARK: - Objects
    var videoArray : [Video]?
    private var selectedVideo : Video?
    var filteredVideos: [Video] = []
    var playerId: String!
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        searchTableView.dataSource = self
        searchTableView.delegate = self
        searchBar.delegate = self
        filteredVideos = []
    }

}

//MARK: - TableViewDataSource
extension SearchViewController: UITableViewDataSource{
    //MARK: - Number of Rows in Section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredVideos.count
    }
    
    //MARK: - Cell For Row at
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.searchTableViewCell, for: indexPath) as? SearchTableViewCell
        let video = filteredVideos[indexPath.row]
        
        cell!.videoTitle.text = video.name
        
        if let posterPath = video.image{
            cell!.posterImage.loadImageAsync(from: posterPath) { image in
                cell!.posterImage.image = image
            }
        }
        return cell!
    }
}

//MARK: - TableViewDelegate
extension SearchViewController: UITableViewDelegate {
    //MARK: - Height For Row at
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return TableViewConstants.SearchAndWatchlist.heightForRowAt
    }
    //MARK: - Did Select Row at
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.selectedVideo = self.videoArray?[indexPath.row].self
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let playerVC = storyboard.instantiateViewController(withIdentifier: "PlayerViewController") as? PlayerViewController {
            let navController = UINavigationController(rootViewController: playerVC)
            navController.modalPresentationStyle = .fullScreen
            playerVC.video = self.selectedVideo
            playerVC.playerId = self.playerId
            playerVC.setNavigationLogo()
            playerVC.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissPlayerViewController))
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    @objc func dismissPlayerViewController() {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: - SearchBarDelegate
extension SearchViewController: UISearchBarDelegate{
    //MARK: - Text Did Change
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            filteredVideos = [] // Prazan niz kada nema teksta za pretragu
        } else {
            filteredVideos = videoArray?.filter { video in
                return video.name!.lowercased().contains(searchText.lowercased())
            } ?? []
        }
        searchTableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            searchBar.resignFirstResponder() // Ovo će sakriti tastaturu
        }
    
    //MARK: - SearchBar Cancel Button Clicked
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.resignFirstResponder()
        filteredVideos = []
        searchTableView.reloadData()
    }
}

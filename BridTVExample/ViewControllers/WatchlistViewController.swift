//
//  WatchlistViewController.swift
//  BridTVExample
//
//  Created by Predrag Jevtic on 10.11.23..
//

//MARK: - Frameworks
import UIKit
import Firebase
import FirebaseStorage
import FirebaseAuth
import SDWebImage

//MARK: - Watchlist ViewController
class WatchlistViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var watchlistTableView: UITableView!
    @IBOutlet weak var watchListLbl: UILabel!
    
    //MARK: - Objects
    var watchlistArray: [Watchlist] = [] {
        didSet {
            if watchlistArray.isEmpty {
                watchListLbl.text = "List is empty"
            } else {
                watchListLbl.text = "Watchlist"
            }
        }
    }
    
    let db = Firestore.firestore()
    var sendVideoData: Video?
    var playerId: String?
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        watchlistTableView.dataSource = self
        watchlistTableView.delegate = self
        loadMovies()
        setNavigationLogo()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadMovies()
    }
    
    //MARK: - Methods
    //Fetch Movies From Firestore
    func loadMovies(){
        if FirebaseAuth.Auth.auth().currentUser != nil {
            self.watchlistArray = WatchlistGlobal.shared.watchlistArray
                DispatchQueue.main.async {
                    self.watchlistTableView.reloadData()
                }
        } else {
            watchlistArray = []
            
            self.watchlistTableView.reloadData()

            showAlert(title: "Not Allowed", message: "Login to see list", buttonTitle: "Go to Login") {
                if let tabBarController = self.tabBarController {
                    tabBarController.selectedIndex = 2
                }
            }
        }
    }
}


//MARK: - TableViewDataSource
extension WatchlistViewController: UITableViewDataSource{
    //MARK: - Number of Rows in Section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.watchlistArray.count
    }
    //MARK: - Cell For Row at
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = watchlistTableView.dequeueReusableCell(withIdentifier: TableViewCells.watchlistTableViewCell, for: indexPath) as? WatchlistTableViewCell else {
            return UITableViewCell()
        }
        cell.movieTitle.text = watchlistArray[indexPath.row].title
        
        let posterURL = watchlistArray[indexPath.row].tubmnailUrl!
        if let url = URL(string: posterURL) {
            cell.movieImage.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
        }
        return cell
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            let documentReference = db.collection("users")
                .document(Auth.auth().currentUser!.uid)
                .collection("watch_list")
                .document((watchlistArray[indexPath.row].videoId)!)
            documentReference.delete() { err in
                if let err = err {
                    print("Error removing document: \(err)")
                } else {
                    print("Document successfully removed!")
                }
            }
            self.watchlistArray.remove(at: indexPath.row)
            
            DispatchQueue.main.async {
                self.watchlistTableView.deleteRows(at: [indexPath], with: .automatic)
            }
            WatchlistGlobal.shared.fetchData()
            
        }
    }
}

//MARK: - TableViewDelegate
extension WatchlistViewController: UITableViewDelegate{
    //MARK: - Height For Row at
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return TableViewConstants.SearchAndWatchlist.heightForRowAt
    }
    //MARK: - Did Select Row at
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        sendVideoData = Video(id: watchlistArray[indexPath.row].videoId, name: watchlistArray[indexPath.row].title, description: watchlistArray[indexPath.row].description, duration: watchlistArray[indexPath.row].duration, image: watchlistArray[indexPath.row].tubmnailUrl)
        playerId = watchlistArray[indexPath.row].playerId
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let playerVC = storyboard.instantiateViewController(withIdentifier: "PlayerViewController") as? PlayerViewController {
            let navController = UINavigationController(rootViewController: playerVC)
            navController.modalPresentationStyle = .fullScreen
            playerVC.video = self.sendVideoData
            playerVC.playerId = self.playerId
            playerVC.setNavigationLogo()
            playerVC.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissPlayerViewController))
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    @objc func dismissPlayerViewController() {
        self.dismiss(animated: true, completion: nil)
    }
    
}

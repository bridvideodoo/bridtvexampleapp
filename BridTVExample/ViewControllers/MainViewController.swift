//
//  ViewController.swift
//  BridTVExample
//
//  Created by Predrag Jevtic on 21.1.22..
//

import UIKit
import AppTrackingTransparency
import JellyGif
import FirebaseAuth
import Firebase

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, PassingDataProtocol {
    
    //MARK: - Model and ViewModel
    
    var allPages: [PagesViewModel]!
    var allData: [Page]!
    var allVideo: [Video]!
    var page: [PageViewModel] = [] {
        didSet {
            collectionView.reloadData()
            let firstIndexPath = IndexPath(item: 0, section: 0)
            if collectionView.numberOfSections > 0 && collectionView.numberOfItems(inSection: 0) > 0 {
                collectionView.scrollToItem(at: firstIndexPath, at: .top, animated: true)
            }
        }
    }
    
    func passAllData(data: [Page]) {
        allData = data.sorted { $0.path! < $1.path! }
        var newAllPages: [PagesViewModel] = []
        
        for (index, page) in allData.enumerated() {
            for (playlistIndex, _) in page.playlists.enumerated() {
                allData[index].playlistsData?[playlistIndex].Playlist.layout = page.playlists[playlistIndex].layout
            }
        }
        
        for page in allData {
            let viewModelPages = createViewModelPages(from: page)
            let pagesViewModel = PagesViewModel(label: page.label, description: page.description, pageViewModel: viewModelPages)
            newAllPages.append(pagesViewModel)
        }
        
        allPages = newAllPages
        extractLabelsFromAllPages()
        
        if let firstPageViewModels = allPages.first?.pageViewModel {
            page = firstPageViewModels
        }
        
    }
    
    func passAllVideoData(data: [Video]) {
        allVideo = data
    }
    
    func createViewModelPages(from page: Page) -> [PageViewModel] {
        var viewModelPages: [PageViewModel] = []
        var lastLayoutType: Layout? = nil
        
        for playlistData in page.playlistsData ?? [] {
            let layout = playlistData.Playlist.layout
            if layout == .grid || layout == .top {
                var isFirstInSeries = (layout != lastLayoutType)
                for video in playlistData.Video {
                    var viewModelPage = PageViewModel(layout: playlistData.Playlist.layout!, name: playlistData.Playlist.name, player: playlistData.Player?.id, videos: [video], video: video)
                    viewModelPage.isFirstOfItsType = isFirstInSeries
                    viewModelPages.append(viewModelPage)
                    isFirstInSeries = false
                }
            } else {
                var viewModelPage = PageViewModel(layout: playlistData.Playlist.layout!, name:playlistData.Playlist.name, player: playlistData.Player?.id, videos: playlistData.Video, video: nil)
                viewModelPage.isFirstOfItsType = (layout != lastLayoutType)
                viewModelPages.append(viewModelPage)
            }
            lastLayoutType = layout
        }
        return viewModelPages
    }
    
    func resetTopNumbersInPageViewModels(_ pageViewModels: inout [PageViewModel]) {
        PageViewModel.resetTopNumber()
        for i in 0..<pageViewModels.count {
            if pageViewModels[i].layout == .top {
                PageViewModel.currentTopNumber += 1
                pageViewModels[i].topNumber = PageViewModel.currentTopNumber
            }
        }
    }
    
    //MARK: - Setting local varibale
    
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var cntAnimationView: UIView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var pageButtonView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var popupImageView: UIImageView!
    @IBOutlet weak var popupTitle: UILabel!
    @IBOutlet weak var popupDescription: UITextView!
    @IBOutlet weak var favoriteBtn: UIButton!
    
    
    //MARK: - Objects
    let buttonHeight: CGFloat = 30
    let spacing: CGFloat = 15
    var allButtons: [UIButton] = []
    var actionPagination = PaginationInfo()
    
    var currentPage = 1
    var allNumberOfPages = 10
    var imageRendition = ImageRendition()
    let db = Firestore.firestore()
    
    let emptyArray: [Video] = []
    var sendVideoData: Video?
    var playerId: String?
    let apiRequest = ApiRequest()
    var tap: UIGestureRecognizer!
    var labels: [String]! {
        didSet {
            let scrollView = UIScrollView()
            scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: buttonHeight + 2 * spacing)
            scrollView.showsHorizontalScrollIndicator = true
            scrollView.isScrollEnabled = true
            pageButtonView.addSubview(scrollView)
            
            var totalButtonWidth: CGFloat = 0
            
            for title in labels {
                let button = UIButton(type: .system)
                button.setTitle(title, for: .normal)
                button.sizeToFit()
                button.frame.size.height = buttonHeight
                totalButtonWidth += button.frame.size.width + spacing
            }
            
            totalButtonWidth -= spacing
            var xOffset: CGFloat = 20
            
            for title in labels {
                let button = UIButton(type: .system)
                button.setTitle(title, for: .normal)
                button.setTitleColor(UIColor.white, for: .normal)
                button.sizeToFit()
                button.frame.origin.x = xOffset
                button.frame.origin.y = 0
                button.frame.size.height = buttonHeight
                button.frame.size.width += 20
                scrollView.addSubview(button)
                xOffset += button.frame.size.width + spacing
                button.backgroundColor = UIColor.darkGray.withAlphaComponent(0.2)
                button.layer.cornerRadius = 10
                button.addTarget(self, action: #selector(buttonTapped(_:)), for: .touchUpInside)
                if title == "Home" {
                    button.backgroundColor = UIColor(red: 240.0/255.0, green: 77.0/255.0, blue: 37.0/255.0, alpha: 0.8)
                }
                allButtons.append(button)
            }
            
            scrollView.contentSize = CGSize(width: totalButtonWidth + 200, height: buttonHeight + 2 * spacing)
        }
    }
    
    func extractLabelsFromAllPages() {
        var extractedLabels: [String] = []
        for pagesViewModel in allPages {
            if let label = pagesViewModel.label, !extractedLabels.contains(label) {
                extractedLabels.append(label)
            }
        }
        labels = extractedLabels
    }
    
    @objc func buttonTapped(_ sender: UIButton) {
        guard let label = sender.title(for: .normal) else { return }
        PageViewModel.resetTopNumber()
        updatePageData(for: label)
        
        if sender.backgroundColor == UIColor(red: 240.0/255.0, green: 77.0/255.0, blue: 37.0/255.0, alpha: 0.8) {
            return
        } else {
            for button in allButtons {
                button.backgroundColor = UIColor.darkGray.withAlphaComponent(0.2)
            }
            sender.backgroundColor = UIColor(red: 240.0/255.0, green: 77.0/255.0, blue: 37.0/255.0, alpha: 0.8)
        }
    }
    
    func updatePageData(for label: String) {
        if let selectedPagesViewModel = allPages.first(where: { $0.label == label }) {
            page = selectedPagesViewModel.pageViewModel ?? []
            resetTopNumbersInPageViewModels(&page)
            
        }
    }
    
    //MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        askPermission()
        
        if FirebaseAuth.Auth.auth().currentUser != nil {
            _ = WatchlistGlobal.shared
        }
        
        if UserDefaults.standard.bool(forKey: "Tutorial") {
            cntAnimationView.isHidden = true
            skipButton.isHidden = true
        } else {
            cntAnimationView.isHidden = false
            skipButton.isHidden = false
            startAnimation()
        }
        tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        overrideUserInterfaceStyle = .dark
        
        apiRequest.delegate = self
        loadConfigData()
        
        setNavigationLogo()
        
        collectionView.register(CaruselCollectionCell.nib(), forCellWithReuseIdentifier: CaruselCollectionCell.identifier)
        collectionView.register(SliderCollectionCell.nib(), forCellWithReuseIdentifier: SliderCollectionCell.identifier)
        collectionView.register(GridCollectionViewCell.nib(), forCellWithReuseIdentifier: GridCollectionViewCell.identifier)
        collectionView.register(TopCollectionViewCell.nib(), forCellWithReuseIdentifier: TopCollectionViewCell.identifier)
        
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (!popupView.isHidden) {
            if WatchlistGlobal.shared.isInWatchList(videoId: (sendVideoData?.id)!) {
                favoriteBtn.setImage(UIImage(systemName: "bookmark.fill"), for: .normal)
            } else {
                favoriteBtn.setImage(UIImage(systemName: "bookmark"), for: .normal)
            }
        }
            
    }
    
    func loadConfigData() {
        apiRequest.getBuilder{ (result) in
            switch result {
            case .success(let model):
                self.apiRequest.config = model
            case .failure(let error):
                debugPrint(error.localizedDescription)
            }
        }
    }
    
    //MARK: - CollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return page.count
    }
    
    //CollectionView cellForItemAt
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell = UICollectionViewCell()
        let item = page[indexPath.row]
        
        switch item.layout {
        case .slider:
            let sliderCell = collectionView.dequeueReusableCell(withReuseIdentifier: SliderCollectionCell.identifier, for: indexPath) as! SliderCollectionCell
            sliderCell.headerTitleLabel.text = item.name
            sliderCell.configure(with: item.videos, playerID: item.player!, channel: "")
            sliderCell.didSelectItemAction = { [weak self] video, playerId in
                self!.sendVideoData = video
                self?.playerId = playerId
                self?.showPopup()
            }
            cell = sliderCell
            break
        case .carusel:
            let caruselCell = collectionView.dequeueReusableCell(withReuseIdentifier: CaruselCollectionCell.identifier, for: indexPath) as! CaruselCollectionCell
            caruselCell.headerTitleLabel.text = item.name
            caruselCell.configure(with: item.videos, playerID: item.player!, channel: "")
            caruselCell.didSelectItemAction = { [weak self] video, playerId in
                self!.sendVideoData = video
                self?.playerId = playerId
                self?.showPopup()
            }
            cell = caruselCell
        case .grid:
            
            let gridCell = collectionView.dequeueReusableCell(withReuseIdentifier: GridCollectionViewCell.identifier, for: indexPath) as! GridCollectionViewCell
            if item.isFirstOfItsType {
                gridCell.headerTitleLabel.text = item.name
                gridCell.headerTitleLabel.isHidden = false
            } else {
                gridCell.headerTitleLabel.isHidden = true
            }
            
            gridCell.configure(with: item.videos.first!, playerID: item.player!)
            gridCell.didSelectItemAction = { [weak self] video, playerId in
                self!.sendVideoData = video
                self?.playerId = playerId
                self?.showPopup()
            }
            cell = gridCell
        case .top:
            let topCell = collectionView.dequeueReusableCell(withReuseIdentifier: TopCollectionViewCell.identifier, for: indexPath) as! TopCollectionViewCell
            if item.isFirstOfItsType {
                topCell.headerTitleLabel.text = item.name
                topCell.headerTitleLabel.isHidden = false
            } else {
                topCell.headerTitleLabel.isHidden = true
            }
            topCell.configure(with: item.videos.first!, number: item.topNumber ,playerID: item.player!)
            topCell.didSelectItemAction = { [weak self] video, playerId in
                self!.sendVideoData = video
                self?.playerId = playerId
                self!.showPopup()
            }
            
            cell = topCell
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let layoutType = page[indexPath.row].layout
        let width = collectionView.bounds.width
        return layoutType.cellSize(for: width)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == Segues.playVideo) {
            let playerVC = segue.destination as! PlayerViewController
//            playerVC.modalPresentationStyle = .fullScreen
            playerVC.video = self.sendVideoData
            playerVC.playerId = self.playerId
        }
        if (segue.identifier == Segues.toSearchVC) {
            let searchVC = segue.destination as! SearchViewController
            searchVC.videoArray = Array(Dictionary(allVideo.compactMap { ($0.id, $0) }, uniquingKeysWith: { (first, _) in first }).values)
            searchVC.playerId = page.first?.player
        }
        
    }
    
    //MARK: - ATTrackingManager
    
    func askPermission() {
        if #available(iOS 14, *) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                ATTrackingManager.requestTrackingAuthorization(completionHandler: { status in
                    switch status {
                    case .notDetermined:
                        print("disable tracking")
                        break
                    case .authorized:
                        print("enable tracking")
                        break
                    case .denied:
                        print("disable tracking")
                        break
                    default:
                        print("disable tracking")
                        break
                    }
                })
            })
        }
    }
    
    //MARK: - Animation tutoral
    
    func startAnimation() {
        let imageView = JellyGifImageView(frame: CGRect(x: 0, y: 0, width: self.cntAnimationView.frame.size.width, height: self.cntAnimationView.frame.height))
        //        if self.traitCollection.userInterfaceStyle == .dark {
        imageView.startGif(with: .name("tutorial_light"))
        //        } else {
        //            imageView.startGif(with: .name("tutorial_light"))
        //        }
        
        self.cntAnimationView .addSubview(imageView)
        UserDefaults.standard.set(true, forKey: "Tutorial")
    }
    
    func showPopup() {
        popupView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        popupView.isHidden = false
        popupView.alpha = 0
        setupPopupView()
        popupView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        
        popupView.addGestureRecognizer(tap)
        popupView.isUserInteractionEnabled = true
        
        if WatchlistGlobal.shared.isInWatchList(videoId: (sendVideoData?.id)!) {
            favoriteBtn.setImage(UIImage(systemName: "bookmark.fill"), for: .normal)
        } else {
            favoriteBtn.setImage(UIImage(systemName: "bookmark"), for: .normal)
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            self.popupView.alpha = 1
            self.popupView.transform = CGAffineTransform.identity
        })
    }
    
    
    func setupPopupView() {
        if sendVideoData?.image != nil {
            popupImageView.downloaded(from: imageRendition.getBestImage(video: sendVideoData!))
        } else {
            popupImageView.image = UIImage(named: "placeholder")
        }
        popupDescription.textContainer.lineBreakMode = .byTruncatingTail
        popupTitle.text = sendVideoData?.name
        popupDescription.text = sendVideoData?.description
        
    }
    
    func hidePopup() {
        UIView.animate(withDuration: 0.5, animations: {
            self.popupView.alpha = 0
            self.popupView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        }, completion: { _ in
            self.popupView.isHidden = true
            self.popupView.removeGestureRecognizer(self.tap)
        })
    }
    
    //MARK: - Action button
    
    @IBAction func skipTutorial(_ sender: Any) {
        self.cntAnimationView.isHidden = true
        self.skipButton.isHidden = true
    }
    
    func presentPlayerViewController() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let playerVC = storyboard.instantiateViewController(withIdentifier: "PlayerViewController") as? PlayerViewController {
            let navController = UINavigationController(rootViewController: playerVC)
            navController.modalPresentationStyle = .fullScreen
            playerVC.video = self.sendVideoData
            playerVC.playerId = self.playerId
            playerVC.setNavigationLogo() // Postavljanje naslova
            playerVC.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissPlayerViewController))
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    @objc func dismissPlayerViewController() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func searchButtonDidPress(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: Segues.toSearchVC, sender: nil)
    }
    @IBAction func playTapped(_ sender: Any) {
        presentPlayerViewController()
//        performSegue(withIdentifier: Segues.playVideo, sender: self)
    }
    
    @IBAction func favoriteTapped(_ sender: Any) {
        if FirebaseAuth.Auth.auth().currentUser == nil {
            showAlert(title: "Not Allowed", message: "Login to save on list", buttonTitle: "Go to Login") {
                if let tabBarController = self.tabBarController {
                    tabBarController.selectedIndex = 2
                }
            }
        } else {
            if (WatchlistGlobal.shared.isInWatchList(videoId: (sendVideoData?.id)!)) {
                favoriteBtn.setImage(UIImage(systemName: "bookmark"), for: .normal)
                let documentReference = db.collection("users").document(Auth.auth().currentUser!.uid).collection("watch_list").document((sendVideoData?.id)!)
                documentReference.delete() { err in
                    if let err = err {
                        print("Error removing document: \(err)")
                    } else {
                        print("Document successfully removed!")
                    }
                    WatchlistGlobal.shared.fetchData()
                }
            } else {
                db.collection("users").document(Auth.auth().currentUser!.uid).collection("watch_list").document((sendVideoData?.id)!).setData([
                    "date_added":  String(DateFormatter.localizedString(from: Date(), dateStyle: .medium, timeStyle: .short)),
                    "description": sendVideoData?.description as Any,
                    "duration": sendVideoData?.duration as Any,
                    "playerId":  self.playerId as Any,
                    "title": sendVideoData?.name as Any,
                    "tubmnailUrl":sendVideoData?.image as Any,
                    "videoId": sendVideoData?.id as Any
                ]) { err in
                    if let err = err {
                        print("Error writing document: \(err)")
                    } else {
                        print("Document successfully written!")
                    }
                }
                favoriteBtn.setImage(UIImage(systemName: "bookmark.fill"), for: .normal)
                WatchlistGlobal.shared.fetchData()
            }
        }
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        hidePopup()
    }
    
}


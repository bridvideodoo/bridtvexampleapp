//
//  ApiRequest.swift
//  BridTVExample
//
//  Created by Predrag Jevtic on 21.1.22..
//

import Foundation

class ApiRequest {
    
    //MARK: - Setting local varibale
    
    let referenceUrl = "https://services.brid.tv/services/get/"
    var playlistsURls: [String] = []
    var playlistsURlsOfRest: [String] = []
    var playlistsData: [PlaylistData] = []
    
    var delegate: PassingDataProtocol?
    private var dataTask: URLSessionDataTask?
    
    var config: [Page]! {
        didSet {
            var newArray: [Page] = []
            for var page in config {
                page.playlistsUrls = fromDataToArray(playlists: page.playlists)
                newArray.append(page)
            }
            allPages = newArray
        }
    }
    
    var allPages: [Page]! {
        didSet {
            for page in allPages {
                getAllValueforPage(page: page) { newPage in
                    self.allData.append(newPage)
                }
            }
        }
    }
    
    var allData: [Page] = [] {
        didSet {
            if allData.count == config.count {
                self.delegate?.passAllData(data: allData)
                self.delegate?.passAllVideoData(data: allData.flatMap { $0.playlistsData ?? [] }.flatMap { $0.Video })
            }
        }
    }
    
    
    func getAllValueforPage(page: Page, completion: @escaping (Page) -> Void) {
        var arrayData: [PlaylistData] = []
        
        fetchURLs(page.playlistsUrls!) { (playlistData) in
            for var data in playlistData {
                for item in page.playlists {
                    data.Playlist.layout = item.layout
                }
            }
            arrayData = playlistData
            if arrayData.count == page.playlistsUrls?.count {
                let newPage = Page(label: page.label,
                                   description: page.description,
                                   type: page.type,
                                   creatorName: page.creatorName,
                                   path: page.path,
                                   playlists: page.playlists,
                                   playlistsData: arrayData,
                                   playlistsURLs: page.playlistsUrls!)
                completion(newPage)
            }
        }
    }
    
    //MARK: - Methods for JSON Data
    func getBuilder(completion: @escaping (Result<[Page], Error>) -> Void) {
        guard let url = Bundle.main.url(forResource: "config", withExtension: "json") else {
            completion(.failure(NSError(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey: "URL not found"])))
            return
        }
        
        do {
            let JSONData = try Data(contentsOf: url)
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            let pages = try decoder.decode([Page].self, from: JSONData)
            let myModel = pages.map {
                Page(label: $0.label,
                     description: $0.description,
                     type: $0.type,
                     creatorName: $0.creatorName,
                     path: $0.path,
                     playlists: $0.playlists,
                     playlistsData: $0.playlistsData,
                     playlistsURLs: $0.playlistsUrls ?? [""])
            }
            
            DispatchQueue.main.async {
                completion(.success(myModel))
            }
        } catch {
            completion(.failure(error))
        }
    }

//    func getBuilder(completion: @escaping (Result<[Page], Error>) -> Void) {
//        guard let url = Bundle.main.url(forResource: "config", withExtension: "json") else {
//            return
//        }
//        
//        do {
//            let JSONData = try Data(contentsOf: url)
//            let decoder = JSONDecoder()
//            decoder.keyDecodingStrategy = .convertFromSnakeCase
//            let pages = try decoder.decode([Page].self, from: JSONData)
//            
//            var myModel: [Page] = []
//            for page in pages {
//                let initializedPage = Page(label: page.label,
//                                           description: page.description,
//                                           type: page.type,
//                                           creatorName: page.creatorName,
//                                           path: page.path,
//                                           playlists: page.playlists,
//                                           playlistsData: page.playlistsData,
//                                           playlistsURLs: page.playlistsUrls ?? [""])
//                myModel.append(initializedPage)
//            }
//            
//            DispatchQueue.main.async {
//                completion(.success(myModel))
//            }
//        } catch {
//            completion(.failure(error))
//        }
//    }
    
//    func getBuilder(completition: @escaping (Result<[Page], Error >) -> Void) {
//        guard let url = Bundle.main.url(forResource: "config", withExtension: "json") else { return }
//        
//        if let JSONData = try? Data(contentsOf: url) {
//            do {
//                let myModel = try! JSONDecoder().decode([Page].self, from: JSONData)
//                DispatchQueue.main.async {
//                    completition(.success(myModel))
//                }
//            }
//        }
//    }
    
    func fromDataToArray(playlists: [Playlist]) -> [String]{
        var arrayPlaylist: [String] = []
        for singlePlaylist in playlists {
            if let playerType = singlePlaylist.type, let  playerId = singlePlaylist.playerId, let playlistId = singlePlaylist.id {
                arrayPlaylist.append(createUrlFromJsonData(type: playerType, playerId: "\(playerId)", playlistId: "\(playlistId)"))
            }
        }
        return arrayPlaylist
    }
    
    func createUrlFromJsonData(type: String, playerId: String, playlistId: String) -> String {
        return "\(referenceUrl)\(type)/\(playerId)/\(playlistId)/1/50/0.json"
    }
    
    func fetchURLs(_ initialURLs: [String], completionHandler: @escaping ([PlaylistData]) -> Void) {
        var pendingURLs = initialURLs
        var playlistsData: [PlaylistData] = [] // Koristi niz za čuvanje podataka
        
        func startNext() {
            guard let urlString = pendingURLs.first else {
                completionHandler(playlistsData)
                return
            }
            pendingURLs.removeFirst()
            guard let url = URL(string: urlString) else { return }
            getJSON(url: url) { (result) in
                switch result {
                case .success(let model):
                    playlistsData.append(model) // Dodaj podatke u niz
                    startNext()
                case .failure(let error):
                    debugPrint(error.localizedDescription)
                }
            }
        }
        startNext()
    }
    
    func getPagingData(forPage page: Int, playerId: Int, playlistId: Int, itemsPerPage: Int, completion: @escaping (Result<PlaylistData, Error>) -> Void) {
        let urlString =  "\(referenceUrl)playlist/\(playerId)/\(playlistId)/\(page)/50/0.json"
        
        if let url = URL(string: urlString) {
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                if let error = error {
                    completion(.failure(error))
                } else if let data = data {
                    do {
                        let decoder = JSONDecoder()
                        let yourModel = try decoder.decode(PlaylistData.self, from: data)
                        completion(.success(yourModel))
                    } catch {
                        completion(.failure(error))
                    }
                }
            }.resume()
        } else {
            completion(.failure(NSError(domain: "Invalid URL", code: 0, userInfo: nil)))
        }
    }
    
    //MARK: - Parsing JSON
    
    func getJSON(url:URL, completition: @escaping (Result<PlaylistData, Error >) -> Void) {
        do {
            Task{@MainActor in
                let (data, _) = try await URLSession.shared.data(from: url)
                if let myModel = try? JSONDecoder().decode(PlaylistData.self, from: data) {
                    DispatchQueue.main.async {
                        completition(.success(myModel))
                    }
                } else {
                    
                }
            }
        }
    }
    
    func getRelatedVideo(playerID: String, videoID: String, completition: @escaping (Result<SingleVideo, Error >) -> Void) {
        guard let url = URL(string: "https://services.brid.tv/services/get/video/\(playerID)/\(videoID).json") else { return }
        
        
        do {
            Task{@MainActor in
                let (data, _) = try await URLSession.shared.data(from: url)
                let myModel = try! JSONDecoder().decode(SingleVideo.self, from: data)
                DispatchQueue.main.async {
                    completition(.success(myModel))
                }
            }
        }
    }
}

protocol PassingDataProtocol {
    func passAllData(data: [Page])
    func passAllVideoData(data: [Video])
}

